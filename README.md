# Threejs-test5

[Watch demo](https://threejs-test5.vercel.app/)

Создать упрощенную версию [three.js editor](https://threejs.org/editor/) :

-   [x] возможность загружать glb, gltf, fbx модели
-   [x] возможность кликнуть по мешу (сделать его активным)
-   [x] сделать обводку для активного меша
-   [x] применить transformControls к нему
-   [] менять основные настройки материала в зависимости от его типа (цвет, прозрачность, металнес, рафнес)
-   [] загрузка и примение map, normalMap
-   [x] применять envMap (картинки и hdr) как ко всей сцене так и к отдельным материалам
