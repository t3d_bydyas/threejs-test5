import * as THREE from 'three'

import Application from '../Application'

export default class Environment {
    constructor() {
        // Setup
        this.application = new Application()
        this.scene = this.application.scene
    }

    setEnvironmentMap(mapTexture, type) {
        this.environmentMap = mapTexture
        if (type === 'hdr') {
            this.environmentMap.mapping = THREE.EquirectangularReflectionMapping
        }
        this.scene.environment = this.environmentMap
    }
}
