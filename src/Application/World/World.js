import Application from '../Application'
import Platform from './Platform'

export default class World {
    constructor() {
        this.application = new Application()

        // Setup
        this.resources = this.application.resources
        this.scene = this.application.scene

        // Listener
        this.resources.on('assets_loaded', () => {
            this.platform = new Platform()
        })
    }
}
