import { TransformControls } from 'three/examples/jsm/controls/TransformControls'

import Application from '../Application'

export default class Models {
    constructor() {
        // Setup
        this.application = new Application()
        this.scene = this.application.scene
        this.camera = this.application.camera
        this.canvas = this.application.canvas
        this.postprocessing = this.application.postprocessing

        this.children = []

        this.changeControlsModeHandler = this.changeControlsMode.bind(this)
        this.disposeHandler = this.dispose.bind(this)

        this.setTransformControls()
    }

    setTransformControls() {
        this.transformControls = new TransformControls(this.camera.instance, this.canvas)
        this.transformControls.addEventListener('dragging-changed', (event) => {
            this.camera.controls.enabled = !event.value
        })
        this.scene.add(this.transformControls)
    }

    changeControlsMode(event) {
        switch (event.code) {
            case 'KeyG':
                this.transformControls.setMode('translate')
                break
            case 'KeyR':
                this.transformControls.setMode('rotate')
                break
            case 'KeyS':
                this.transformControls.setMode('scale')
                break
        }
    }

    add(obj, ext) {
        let model
        switch (ext) {
            case 'gltf':
            case 'glb':
                model = obj.scene
                break
            case 'fbx':
                model = obj
                break
            default:
                break
        }
        this.children.push(model)
        this.scene.add(model)
    }

    setActive(obj) {
        this.active = obj
        this.transformControls.attach(this.active)
        window.addEventListener('keydown', this.changeControlsModeHandler)
        window.addEventListener('keydown', this.disposeHandler)
        this.postprocessing.outlinePass.setSelected(this.active)
    }

    removeActive() {
        this.active = null
        this.transformControls.detach()
        window.removeEventListener('keydown', this.changeControlsModeHandler)
        window.removeEventListener('keydown', this.disposeHandler)
        this.postprocessing.outlinePass.clearSelected()
    }

    dispose(event) {
        if (event.code === 'Delete') {
            this.scene.remove(this.active)
            this.children.splice(this.children.indexOf(this.active), 1)
            this.removeActive()
        }
    }
}
