import * as THREE from 'three'

import Application from '../Application'

export default class Platform {
    constructor() {
        this.application = new Application()

        // Setup
        this.scene = this.application.scene
        this.size = 10
        this.divisions = 10

        this.setGrid()
    }

    setGrid() {
        this.platform = new THREE.GridHelper(this.size, this.divisions)
        this.scene.add(this.platform)
    }
}
