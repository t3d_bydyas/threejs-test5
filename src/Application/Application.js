import * as THREE from 'three'

import Sizes from './utils/Sizes.js'
import Time from './utils/Time.js'
import Resources from './utils/Resources.js'
import Camera from './Camera.js'
import Renderer from './Renderer.js'

import assets from './assets.js'
import World from './World/World.js'
import Environment from './World/Environment.js'
import DragAndDropUploading from './addons/DragAndDropUploading.js'
import TipsPopup from './ui/TipsPopup.js'
import Postprocessing from './Post-processing/Postprocessing.js'
import Raycaster from './Raycaster.js'
import Menu from './ui/Menu.js'

THREE.ColorManagement.enabled = false

export default class Application {
    static instance

    constructor(canvas) {
        // Singleton
        if (Application.instance) {
            return Application.instance
        }
        Application.instance = this

        // Global access
        window.application = this

        // Options
        this.canvas = canvas

        // Setup
        this.sizes = new Sizes()
        this.time = new Time()
        this.scene = new THREE.Scene()
        this.camera = new Camera()
        this.renderer = new Renderer()
        this.postprocessing = new Postprocessing()
        this.environment = new Environment()
        this.resources = new Resources(assets)
        this.world = new World()
        this.tips = new TipsPopup()
        this.raycaster = new Raycaster()
        this.dragAndDropUploading = new DragAndDropUploading()
        this.menu = new Menu()

        // Listeners
        this.sizes.on('resize', () => this.resize())
        this.time.on('tick', () => this.update())
    }

    resize() {
        this.camera.resize()
        this.renderer.resize()
        this.postprocessing.resize()
    }

    update() {
        this.camera.update()
        this.renderer.update()
        this.tips.update()
        this.postprocessing.update()
    }
}
