import { EffectComposer } from 'three/addons/postprocessing/EffectComposer.js'
import { RenderPass } from 'three/addons/postprocessing/RenderPass.js'

import Application from '../Application'
import MyOutlinePass from './MyOutlinePass'

export default class Postprocessing {
    constructor() {
        // Setup
        this.application = new Application()
        this.scene = this.application.scene
        this.renderer = this.application.renderer.instance
        this.camera = this.application.camera.instance
        this.sizes = this.application.sizes

        this.setComposer()
        this.setRenderPass()

        this.outlinePass = new MyOutlinePass(this.composer)
    }

    setComposer() {
        this.composer = new EffectComposer(this.renderer)
    }

    setRenderPass() {
        this.renderPass = new RenderPass(this.scene, this.camera)
        this.composer.addPass(this.renderPass)
    }

    resize() {
        this.composer.setSize(this.sizes.width, this.sizes.height)
    }

    update() {
        this.composer.render()
    }
}
