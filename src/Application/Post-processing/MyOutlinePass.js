import * as THREE from 'three'
import { OutlinePass } from 'three/addons/postprocessing/OutlinePass.js'

import Application from '../Application'

export default class MyOutlinePass {
    constructor(composer) {
        // Options
        this.composer = composer

        // Setup
        this.application = new Application()
        this.sizes = this.application.sizes
        this.camera = this.application.camera.instance
        this.scene = this.application.scene

        this.setPass()
    }

    setPass() {
        this.outlinePass = new OutlinePass(
            new THREE.Vector2(this.sizes.width, this.sizes.height),
            this.scene,
            this.camera
        )

        this.outlinePass.edgeStrength = 3.0
        this.outlinePass.edgeGlow = 1.0
        this.outlinePass.edgeThickness = 3.0
        this.outlinePass.pulsePeriod = 0
        this.outlinePass.usePatternTexture = false
        this.outlinePass.visibleEdgeColor.set(0xffff00)
        this.outlinePass.hiddenEdgeColor.set(0xffff00)

        this.composer.addPass(this.outlinePass)
    }

    setSelected(obj) {
        this.outlinePass.selectedObjects = [obj]
    }

    clearSelected() {
        this.outlinePass.selectedObjects = []
    }
}
