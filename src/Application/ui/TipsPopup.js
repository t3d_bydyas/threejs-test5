import Application from '../Application'

export default class TipsPopup {
    constructor() {
        // Setup
        this.application = new Application()
        this.resources = this.application.resources

        this.popup = document.querySelector('#popup')
        this.body = this.popup.children[1]

        // Listener
        this.popup.addEventListener('click', () => this.closePopup())
        this.resources.on('assets_loaded', () => this.showModalUploadingTip())
    }

    closePopup() {
        this.popup.classList.remove('show')
    }

    showModalUploadingTip() {
        const content = 'Drag-n-drop your models to add them'
        this.body.append(content)
        this.popup.classList.add('show')
    }

    update() {
        if (this.resources.models.children.length) {
            this.closePopup()
        }
    }
}
