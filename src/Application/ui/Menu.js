import Application from '../Application'

export default class Menu {
    constructor() {
        // Setup
        this.application = new Application()
        this.resources = this.application.resources

        // DOM
        this.inputSceneEnvMap = document.querySelector('input[name="sceneEnvMap"]')

        // Listener
        this.inputSceneEnvMap.addEventListener('change', (event) => this.handleEnvMapChange(event))
    }

    handleEnvMapChange(event) {
        event.preventDefault()

        if (event.target.files.length) {
            const file = event.target.files[0]
            const extension = file.name.split('.')[1]
            const path = URL.createObjectURL(file)
            this.resources.load(path, extension)
        }
    }
}
