import * as THREE from 'three'
import Application from './Application'

export default class Raycaster {
    constructor() {
        // Setup
        this.application = new Application()
        this.resources = this.application.resources
        this.camera = this.application.camera.instance
        this.sizes = this.application.sizes
        this.canvas = this.application.canvas

        this.mouse = {}

        this.setInstance()

        // Listeners
        this.canvas.addEventListener('click', (event) => this.onMouseClick(event))
    }

    setInstance() {
        this.instance = new THREE.Raycaster()
    }

    onMouseClick(event) {
        this.mouse.x = (event.clientX / this.sizes.width) * 2 - 1
        this.mouse.y = -(event.clientY / this.sizes.height) * 2 + 1

        this.checkIntersection()
    }

    checkIntersection() {
        this.instance.setFromCamera(this.mouse, this.camera)
        const intersects = this.instance.intersectObjects(this.resources.models.children, true)

        if (intersects.length > 0) {
            const selectedObject = intersects[0].object

            const getParent = (object) => {
                if (object.parent.type === 'Scene') {
                    return object
                } else {
                    return getParent(object.parent)
                }
            }
            let parent = getParent(selectedObject)

            this.resources.models.setActive(parent)
        } else {
            this.resources.models.removeActive()
        }
    }
}
