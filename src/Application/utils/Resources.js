import * as THREE from 'three'
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js'
import { RGBELoader } from 'three/addons/loaders/RGBELoader.js'
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'

import EventEmitter from './EventEmitter.js'
import Models from '../World/Models.js'
import Application from '../Application.js'

export default class Resources extends EventEmitter {
    constructor(assets) {
        super()

        // Options
        this.assets = assets

        // Setup
        this.application = new Application()
        this.environment = this.application.environment

        this.items = {}
        this.toLoad = this.assets.length
        this.loaded = 0
        this.models = new Models()

        this.setLoaders()
        this.assetsLoading()
    }

    setLoaders() {
        this.loaders = {}
        this.loaders.gltfLoader = new GLTFLoader()
        this.loaders.textureLoader = new THREE.TextureLoader()
        this.loaders.rgbeLoader = new RGBELoader()
        this.loaders.fbxLoader = new FBXLoader()
    }

    load(path, extension) {
        // Enable uploading only when all assets have been loaded
        const ext = extension.toLowerCase()
        if (this.loaded === this.toLoad) {
            switch (ext) {
                case 'gltf':
                case 'glb':
                    this.loaders.gltfLoader.load(path, (obj) => this.models.add(obj, ext))
                    break
                case 'fbx':
                    this.loaders.fbxLoader.load(path, (obj) => this.models.add(obj, ext))
                    break
                case 'hdr':
                    this.loaders.rgbeLoader.load(path, (obj) => this.environment.setEnvironmentMap(obj, 'hdr'))
                    break
                default:
                    throw new Error(`Unexpected file extension (${extension})`)
            }
        }
    }

    assetsLoading() {
        for (const asset of this.assets) {
            switch (asset.type) {
                case 'texture':
                    this.loaders.textureLoader.load(asset.path, (file) => this.assetsLoaded(asset, file))
                    break
                case 'hdrTexture':
                    this.loaders.rgbeLoader.load(asset.path, (file) => this.assetsLoaded(asset, file))
                    break
                default:
                    throw new Error(`Unexpected asset type (${asset.type})`)
            }
        }
    }

    assetsLoaded(asset, file) {
        this.items[asset.name] = file

        this.loaded++

        if (this.loaded === this.toLoad) {
            this.trigger('assets_loaded')
        }
    }
}
