import Application from '../Application'

export default class DragAndDropUploading {
    constructor() {
        // Setup
        this.application = new Application()
        this.resources = this.application.resources

        // DOM
        this.dropZone = document.querySelector('#drop_zone')

        // Listeners
        this.dropZone.addEventListener('dragenter', (event) => this.highlightDropZone(event), false)
        this.dropZone.addEventListener('dragover', (event) => this.highlightDropZone(event), false)
        this.dropZone.addEventListener('dragleave', (event) => this.unhighlightDropZone(event), false)
        this.dropZone.addEventListener(
            'drop',
            (event) => {
                this.unhighlightDropZone(event)
                this.handleDrop(event)
            },
            false
        )
    }

    highlightDropZone(event) {
        event.preventDefault()
        this.dropZone.classList.add('highlight')
    }

    unhighlightDropZone(event) {
        event.preventDefault()
        this.dropZone.classList.remove('highlight')
    }

    handleDrop(event) {
        event.preventDefault()

        if (event.dataTransfer.files) {
            ;[...event.dataTransfer.files].forEach((file) => {
                const extension = file.name.split('.')[1]
                const path = URL.createObjectURL(file)
                this.resources.load(path, extension)
            })
        }
    }
}
